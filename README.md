docker build  . --tag cellar_seta_airflow:latest

if "ERROR: failed to solve: error getting credentials - err: exit status 1, out: `GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown: The name org.freedesktop.secrets was not provided by any .service files`"

sudo apt install gnupg2 pass

for first time run:
docker compose up airflow-init
or :
docker compose up 


# add correct minio keys


docker compose up -d --no-deps --build airflowebserver airflow-scheduler

, to init airflow after buiding new image I've executed:
docker compose down --volumes --remove-orphans
docker compose up airflow-init

BUT be careful, because it will remove all previous data from airflow db
see: https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html


# run scripts 
python3 dags/tasks/download_documents.py --file_ids=dags/data/10_documents_uri_to_test.csv