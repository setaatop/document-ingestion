# Base image
FROM apache/airflow:2.6.0-python3.10

# Switch to root user to install Java and other dependencies
USER root

# Install OpenJDK-11 and Ant
RUN apt update && \
    apt-get install -y openjdk-11-jdk ant && \
    apt-get clean

# Set JAVA_HOME environment variable
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64/

# Create app directory, set it as working directory and give permissions to airflow user
RUN mkdir -p /opt/airflow && \
    chown -R airflow /opt/airflow

# Switch to airflow user
USER airflow

# Set the working directory
WORKDIR /opt/airflow

# Install Python dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copy application code into the working directory
COPY . .

# This Dockerfile assumes the application code and requirements.txt
# are in the same directory as the Dockerfile.
