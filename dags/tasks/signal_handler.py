import signal

class SignalHandler:
    def __init__(self):
        self.INTERRUPTED = False
        signal.signal(signal.SIGINT, self.handle_signal)
        signal.signal(signal.SIGTERM, self.handle_signal)

    def handle_signal(self, signal_number, frame):
        self.INTERRUPTED = True
        print("\nSignal received. Stopping execution...")

    def shutdown(self, executor, futures):
        print("\nShutting down executor...")
        for future in futures:
            future.cancel()
        executor.shutdown(wait=True)
        print("Executor shutdown complete.")

    def check_interrupted(self):
        return self.INTERRUPTED