# process_documents.py
import os


try:
    from tasks.tools import Tools
except ImportError:
    from tools import Tools

try:
    from tasks.document_handler import DocumentHandler
except ImportError:
    from document_handler import DocumentHandler

from tqdm import tqdm
import argparse


from tika import parser
import PyPDF2
from minio.error import S3Error
from io import BytesIO
import pandas as pd

LOG_FILE_NAME = 'process_documents.log'
PROCESSED_DOCUMENTS_DIR = 'processed_documents'
TRIMMED_DOCUMENTS_DIR = 'trimmed_documents'


class ProcessDocumentsHandler(DocumentHandler):
    def __init__(self):
        super().__init__()
        self.logger = self.setup_logging(LOG_FILE_NAME)

    def process_document(self, work_id, max_pages):
        
        work_documents = self.grouped_documents_by_work[work_id]
        
        work_content = self.prepare_work_content(work_id, work_documents, max_pages)
        
        try:
            # Process the document using Tika
            text = Tools.process_document_using_tika(work_id, work_content.getvalue(), self.logger)

            if not text or len(text.strip()) < 100:
                self.logger.error(f"File with insufficient text content: {work_id}")
                self.logger.error(f"Document content: {work_content}")
                return None

            # Save the processed data to a BytesIO object
            processed_data = BytesIO(text.encode('utf-8'))
            
            processed_document_name = f"{work_id}.txt"

            # Upload the processed data back to MinIO
            self.minioClient.put_object(DocumentHandler.PROCESSED_DOCUMENTS_BUCKET, processed_document_name, processed_data, length=processed_data.getbuffer().nbytes)

            self.logger.info(f"Successfuly processed file: {work_id}")

        except Exception as e:
            self.logger.error(f"Error processing file: {work_id}, Error: {str(e)}", exc_info=True)


    def prepare_work_content(self,work_id, work_documents, max_pages):
            """
            Prepare a document for processing by trimming it and merging it with other documents if necessary.
            """
            if len(work_documents) > 1:
                work_content = self.merge_documents(work_id, work_documents, max_pages)
            else:
                if max_pages > 0:
                    work_content = self.keep_pages(work_documents[0], max_pages, DocumentHandler.DOWNLOADED_DOCUMENTS_BUCKET, DocumentHandler.REDUCED_DOCUMENTS_BUCKET)
                else :
                    try:
                        response = self.minioClient.get_object(DocumentHandler.DOWNLOADED_DOCUMENTS_BUCKET, work_documents[0])
                        work_content = BytesIO(response.data)

                    except S3Error as err:
                        self.logger.info(f"Erro processing file {work_documents[0]}")
                        return False

            return work_content
        
    def merge_documents(self,work_id, work_documents, max_pages):
        
        # check if all docs were downloaded
        all_docs_are_downloaded = True
        documents = dict()
        for doc in work_documents:
            try:
                # self.minioClient.stat_object(DocumentHandler.DOWNLOADED_DOCUMENTS_BUCKET, doc)
                response = self.minioClient.get_object(DocumentHandler.DOWNLOADED_DOCUMENTS_BUCKET, doc)
                documents[doc] = BytesIO(response.data)                 
                
            except S3Error as err:
                all_docs_are_downloaded = False
                return False
        
        
        if all_docs_are_downloaded == True: 
            try :
                self.logger.info(f"Merging {work_documents} documents for work {work_id}")
                # TODO : if max_pages == first_doc_total_pages return first_doc
                first_doc = documents[work_documents[0]]
                reader = PyPDF2.PdfReader(first_doc)
                first_doc_total_pages = len(reader.pages)
                
                if first_doc_total_pages >= max_pages :
                    writer = PyPDF2.PdfWriter()
                    for i in range(max_pages):
                        writer.add_page(reader.pages[i])
                    
                    temp_file = BytesIO()
                    writer.write(temp_file)

                    # Reset the BytesIO object's cursor to the beginning
                    temp_file.seek(0)
                    self.minioClient.put_object(DocumentHandler.REDUCED_DOCUMENTS_BUCKET, f"{work_id}.pdf", temp_file, length=temp_file.getbuffer().nbytes)
                    return temp_file
                
                else :    
                    writer = PyPDF2.PdfWriter()

                    for i in range(first_doc_total_pages):
                        writer.add_page(reader.pages[i])
                    
                    for doc_id,doc in documents.items():
                        if doc != first_doc:
                            reader = PyPDF2.PdfReader(doc)
                            total_pages_doc = len(reader.pages)
                            for i in range(total_pages_doc):
                                writer.add_page(reader.pages[i])
                                total_pages = len(writer.pages)
                                if total_pages >= max_pages:
                                    # Save the limited-pages file to a BytesIO object
                                    temp_file = BytesIO()
                                    writer.write(temp_file)

                                    # Reset the BytesIO object's cursor to the beginning
                                    temp_file.seek(0)

                                    # Upload limited-pages file back to MinIO
                                    self.minioClient.put_object(DocumentHandler.REDUCED_DOCUMENTS_BUCKET, f"{work_id}.pdf", temp_file, length=temp_file.getbuffer().nbytes)

                                    return temp_file
            except Exception as e: 
                self.logger.error(f"Error merging files: {work_id}, Error: {str(e)}", exc_info=True)
        

            return first_doc
    
    def keep_pages(self, document_name, page_limit, bucket_name, new_bucket_name):
            
        try:
            self.logger.info(f"Reducing {document_name}")
            # Download file from MinIO
            response = self.minioClient.get_object(bucket_name, document_name)
            
            # Read file data with PyPDF2
            reader = PyPDF2.PdfReader(BytesIO(response.data))
            total_pages = len(reader.pages)

            if page_limit >= total_pages:
                return BytesIO(response.data)

            
            writer = PyPDF2.PdfWriter()

            for i in range(page_limit):
                writer.add_page(reader.pages[i])

            # Save the limited-pages file to a BytesIO object
            temp_file = BytesIO()
            writer.write(temp_file)

            # Reset the BytesIO object's cursor to the beginning
            temp_file.seek(0)

            # Upload limited-pages file back to MinIO
            # self.minioClient.put_object(new_bucket_name, document_name, temp_file, length=temp_file.getbuffer().nbytes)

            return temp_file

        except Exception as err:
            print(f"Error reducing file {document_name}: {err}")
            return None



    def process_documents(self, documents_names, max_workers, max_pages):
        self.logger.info(f"Processing {len(documents_names)} documents")
        self.process_documents_list(documents_names, self.process_document, max_workers, max_pages)

    def add_arguments(self, parser):
        parser.add_argument('--max_pages', type=int, default=os.getenv("PAGES", 10), help='Number of pages to keep before processing')
        parser.add_argument('--merge_documents', type=bool, default=True, help="Merge documents with the same (cellar) id")
        parser.add_argument('--documents_to_process', type=str, default=None, help="Path to the CSV file containing file IDs")
        parser.add_argument('--num_documents_to_process', type=int, default=0, help="Number of documents to process")
        parser.add_argument('--force', type=bool, default=False, help="Force processing of already processed files")
        # return parser.parse_args()
    
    def process_command_line_arguments(self):
        parser = argparse.ArgumentParser(description='Process documents')
        self.add_arguments(parser)
        super().add_arguments(parser)
        return parser.parse_args()
    
    def get_documents_to_process(self, args, logger):
        works_to_process = []
        logger.info(f"Collecting proceeded file names from MinIO bucket")

        works = self.grouped_documents_by_work
        
        for work_id, documents in tqdm(works.items(), desc="Collecting documents names from MinIO bucket"):
            
            # Get the processed file path
            processed_file_name = work_id + '.txt'
            
            try:
                # Check if the file has been processed
                self.minioClient.stat_object(DocumentHandler.PROCESSED_DOCUMENTS_BUCKET, processed_file_name)
                logger.info(f"Skipping already processed work: {work_id}")
                continue
            except S3Error as err:
                pass
            
            works_to_process.append(work_id)
            if args.num_documents_to_process > 0 and len(works_to_process) >= args.num_documents_to_process:
                break
        
        return works_to_process

    def group_documents_by_work(self, documents_ids = None):
        # TODO: load from a specific file?
        # if documents_ids is None:
        #     documents_ids = self.documents_to_process
        documents_to_download_file = os.path.join(DocumentHandler.DATA_DIR, DocumentHandler.DOCS_TO_DOWNLOAD_FILE)
        self.documents_grouped_by_work_df = pd.read_csv(documents_to_download_file)
        # update all file names, convert c9055101-5906-4b1c-a262-0368bf52672c.0003.05/DOC_1 to downloaded_documents/c9055101-5906-4b1c-a262-0368bf52672c.0003.05_DOC_1.pdf
        self.documents_grouped_by_work_df['doc_file'] = self.documents_grouped_by_work_df['doc_url'].apply(lambda x: x.split("cellar/")[1].replace('/', '_') + '.pdf')
        self.documents_grouped_by_work_df['work_id'] = self.documents_grouped_by_work_df['doc_file'].apply(lambda x: x.split('.')[0])

        
        self.grouped_documents_by_work = self.grouped_documents_by_work = self.documents_grouped_by_work_df.groupby('work_id')['doc_file'].apply(list).to_dict()
        return self.grouped_documents_by_work
        
        

def main():
    process_documents_handler = ProcessDocumentsHandler()
    args = process_documents_handler.process_command_line_arguments()
    
    process_documents_handler.group_documents_by_work()

    if args.documents_to_process:
        if args.num_documents == 0:
            documents_to_process = process_documents_handler.read_csv(args.documents_to_process)
            
        else:
            documents_to_process = process_documents_handler.read_csv(args.documents_to_process)[:args.num_documents]
    else:
        # process files in the downloaded_documents bucket
        documents_to_process = process_documents_handler.get_documents_to_process(args, process_documents_handler.logger)
            
    process_documents_handler.documents_to_process = documents_to_process

    process_documents_handler.process_documents(documents_to_process, args.max_workers, args.max_pages)


if __name__ == "__main__":
   main()