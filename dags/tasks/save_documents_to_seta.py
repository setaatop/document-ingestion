import argparse
import ast
from io import BytesIO
import os
import time
import json


import requests
from requests.exceptions import RequestException


try:
    from tasks.document_handler import DocumentHandler
except ImportError:
    from document_handler import DocumentHandler

import pandas as pd
from elasticsearch import Elasticsearch


import schedule
import threading

try:
    from tasks.tools import Tools
except ImportError:
    from tools import Tools

LOG_FILE_NAME = 'import_documents_to_seta.log'
SETA_DOCUMENTS_DIR = 'seta_documents'
PROCESSED_TRIMMED_DIR = 'processed_trimmed_documents '


class Config:
    API_URL = "http://seta-api:8081/seta-api/api/v1/"
    LOG_DIR = 'logs'
    LOG_FILE_NAME = '6_save_documents_in_SeTA.log'
    DATA_DIR = 'data'
    DOWNLOADED_DOCUMENTS_DIR = 'downloaded_documents'
    SETA_DOCUMENTS_DIR = 'seta_documents'
    PROCESSED_TRIMMED_DIR = 'processed_trimmed_documents'
    SETA_DOCS_INDEX = "seta-embedding-000005"
    DOCUMENTS_METADATA_PATH = 'metadata/documents_metadata_cleaned.csv'

    

class ImportToSetaHandler(DocumentHandler):
    
    def __init__(self):
        super().__init__()
        self.logger = self.setup_logging(LOG_FILE_NAME)
        
    def process_document(self, work_id,es):
        
        work_file_name = f"{work_id}.txt"
        text = self.document_content(work_file_name)
        
        title, dates = self.get_metadata(work_id)
        
        if title is None or dates is None :
            self.logger.warning(f"Skipping docuemnt {work_id}, not title or dates.")
            return
        

        text_embedding = title + '\n' + text


        embedding = self.compute_embedding_vector(text_embedding)
        
        
        seta_doc = self.prepare_seta_doc(work_id, text, title, dates, embedding)
        
        # self.store_seta_doc(es, Config.SETA_DOCS_INDEX, f"cellar:{work_id}", seta_doc)
        
        
    def store_seta_doc(self,es, index, id, doc, replace=False):
        """
        Store a document in the given index of the Elasticsearch instance. If replace is True, will replace the existing document.
        """
        if es.exists(index=index, id=id):
            if replace:
                self.logger.info(f"Replacing document with id {id} in index {index}.")
                res = es.index(index=index, id=id, body=doc)
            else:
                self.logger.warning(f"Document with id {id} already exists in index {index}.")
                return
        else:
            res = es.index(index=index, id=id, body=doc)
            self.logger.info(f"Stored document with id {id} in index {index}.")
        return res
        
    def prepare_seta_doc(self,work_id, text, title, dates, embedding):
        """
        Prepare a document to be stored in the Elasticsearch instance.
        """
        # timestamp format 2019-12-03T11:43:18.617Z
        timestamp = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', time.localtime(time.time()))
        doc = {'date': dates,
                'agent': ['Author: Cellar-SeTA'],
                'subject': [],
                'eurovoc': [],
                'abstract': '',
                'source': 'bookshop',
                'longid': f"cellar:{work_id}",
                'id': work_id,
                'title' : title,
                'doctype': [],
                '@timestamp': timestamp,
                'sbert_vector': embedding,
                'scope': '',
                'ia': 'no',
                '@version': '1',
                'step': 'done',
                'links': [],
                "text" : text
                }

        return doc
    
    def remove_none_dates(self,date_string):
        # Convert string to list
        date_list = ast.literal_eval(date_string)

        # Filter out 'None' values
        date_list = [date for date in date_list if date != 'None' and date != None]

        return date_list
    
    def get_metadata(self, work_id):
        dates = []
        metadata_df = self.documents_grouped_by_work_df[self.documents_grouped_by_work_df['work_id']==work_id]
        if metadata_df.shape[0] == 0:
            self.logger.error(f"Metadata not found for work_id {work_id}")
            return None, None
        # if title is found
        if metadata_df['title'].values[0] != 'None':
            title = metadata_df['title'].values[0]
        else:
            title = None
            self.logger.error(f"Title not found for work_id {work_id}")
        
        if metadata_df['creationDate'].values[0] != 'None':
            creation_date = metadata_df['creationDate'].values[0]
            dates.append(creation_date)
        else:
            creation_date = None
            self.logger.error(f"Creation date not found for work_id {work_id}")
        if metadata_df['lastModificationDate'].values[0] != 'None':
            last_modification_date = metadata_df['lastModificationDate'].values[0]
            dates.append(last_modification_date)
        else:
            last_modification_date = None
        return title, dates
    
    
    def compute_embedding_vector(self,text):
        """
        Compute the embeddings vector for the given text.
        """
        text = Tools.sentenced(text)
        if len(text) > 5000:
            text = text[:5000]

        payload = {
            "text": text,
        }

        r = requests.post(f"{Config.API_URL}compute_embeddings", params=payload, headers=self.headers(token_json))
        r_json = r.json()
        return r_json["embeddings"]["vector"]
    
    def headers(self,token_json):
        """
        Create headers for authenticated requests.
        """
        return {"Authorization": token_json["access_token"]}
    
    
    def process_documents(self, documents_names, max_workers,es):
        self.logger.info(f"Saving into SeTA {len(documents_names)} documents")
        self.process_documents_list(documents_names, self.process_document, max_workers,es)
    
    def group_documents_by_work(self, documents_ids = None):
        # TODO: load from a specific file?
        # if documents_ids is None:
        #     documents_ids = self.documents_to_process
        documents_to_download_file = os.path.join(DocumentHandler.DATA_DIR, DocumentHandler.DOCS_TO_DOWNLOAD_FILE)
        self.documents_grouped_by_work_df = pd.read_csv(documents_to_download_file)
        # "http://publications.europa.eu/resource/cellar/24cf4a94-efbf-11ed-a05c-01aa75ed71a1 => 24cf4a94-efbf-11ed-a05c-01aa75ed71a1
        self.documents_grouped_by_work_df["work_id"] = self.documents_grouped_by_work_df["work"].apply(lambda x: x.split("/")[-1])
        # do not group ? 
        # self.documents_grouped_by_work_df = self.documents_grouped_by_work_df.groupby('work_id').first()[['title', 'creationDate','lastModificationDate']]
        
        return self.documents_grouped_by_work_df
        

    def document_content(self, work_id):
        response = self.minioClient.get_object(DocumentHandler.PROCESSED_DOCUMENTS_BUCKET, work_id)
        work_content = BytesIO(response.data)
        return work_content.getvalue().decode('utf-8')
    
    def document_exists_in_seta(self,es, index, cellar_id):
        """
        Check if a document with the given cellar_id exists in the Elasticsearch index.
        """
        return es.exists(index=index, id=f"cellar:{cellar_id}")

    def get_documents_to_process(self, args, logger,es):
        """
            Collect the paths to all files located in the MinIO bucket that are not already in SeTA.
         """
        docuemnts_to_process = []
        documents = self.minioClient.list_objects(DocumentHandler.PROCESSED_DOCUMENTS_BUCKET, recursive=True) 

        for document in documents:
            document_name = document.object_name
            
            if document_name.endswith('.txt'):
                work_id = document_name.split('.')[0]

                if not self.document_exists_in_seta(es, Config.SETA_DOCS_INDEX, work_id):
                    docuemnts_to_process.append(work_id)

        return docuemnts_to_process
    
    def add_arguments(self, parser):
        parser.add_argument('--documents_to_process', type=str, default=None, help="Path to the CSV file containing file IDs")
        parser.add_argument('--num_documents_to_process', type=int, default=0, help="Number of documents to process")
        parser.add_argument('--force', type=bool, default=False, help="Force processing of already processed files")
        
    
    def process_command_line_arguments(self):
        parser = argparse.ArgumentParser(description='Process documents')
        self.add_arguments(parser)
        super().add_arguments(parser)
        return parser.parse_args()
    
    def setup_elasticsearch(self):
        """
        Setup an Elasticsearch instance.
        """
        return Elasticsearch(['http://seta-es:9200/'], verify_certs=True)
    
    def get_token(self):
        """
        Retrieve a token for authentication.
        """
        try:
            guest_token = requests.get(f"{Config.API_URL}get-token")
        
        except RequestException as e:
            self.logger.error(f"An error occurred while getting SeTA Token: {e}")
            return None  # Or handle the exception in some other way
        
        try:
            token_json = json.loads(guest_token.text)
        
        except json.JSONDecodeError as e:
            self.logger.error(f"An error occurred while decoding the JSON: {e}")
            return None  # Or handle the exception in some other way
        
        return token_json
    
    def refresh_token(self):
        global token_json  # Make sure you're using the global variable
        token_json = self.get_token()  # Refresh the token
        self.logger.info("SeTA Token refreshed.")  # Optional logging

def run_schedule():
    while True:
        schedule.run_pending()
        time.sleep(1)

def main():
    seta_documents_handler = ImportToSetaHandler()
    args = seta_documents_handler.process_command_line_arguments()
    
    seta_documents_handler.group_documents_by_work()
    
    es = seta_documents_handler.setup_elasticsearch()
    
    global token_json  
    token_json = seta_documents_handler.get_token()
    
    if not token_json:
        raise ValueError('Error getting SeTA Token')
    
    schedule.every(55).minutes.do(seta_documents_handler.refresh_token)
    
    # Start a background thread for the scheduler
    schedule_thread = threading.Thread(target=run_schedule)
    schedule_thread.start()

    if args.documents_to_process:
        if args.num_documents == 0:
            documents_to_process = seta_documents_handler.read_csv(args.documents_to_process)

        else:
            documents_to_process = seta_documents_handler.read_csv(args.documents_to_process)[:args.num_documents]
    else:
        # process files in the downloaded_documents bucket
        documents_to_process = seta_documents_handler.get_documents_to_process(args, seta_documents_handler.logger,es)

    seta_documents_handler.documents_to_process = documents_to_process

    seta_documents_handler.process_documents(documents_to_process, args.max_workers,es)


if __name__ == "__main__":
    main()


