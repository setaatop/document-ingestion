import io
import os
from SPARQLWrapper import JSON, CSV, SPARQLWrapper
import requests
import time
import re
import argparse
import datetime
import hashlib






from minio.error import S3Error

# To ba able run script from command line or from airflow
try:
    from tasks.document_handler import DocumentHandler
except ImportError:
    from document_handler import DocumentHandler


LOG_FILE_NAME = 'get_documents_to_download.log'
SPARQL_ENDPOINT = "http://publications.europa.eu/webapi/rdf/sparql"



class GetDocumentToDownloadHandler(DocumentHandler):
    def __init__(self,documents_to_download_file = DocumentHandler.DOCS_TO_DOWNLOAD_FILE):
        super().__init__()
        self.logger = self.setup_logging(LOG_FILE_NAME)
        self.documents_to_download_file = os.path.join(self.DATA_DIR, documents_to_download_file)
        

    def extract_file_id_and_doc_number(self, file_uri):
        match = re.match(r"http://publications.europa.eu/resource/cellar/([^/]+)/([^/]+)", file_uri)
        if match:
            return match.group(1), match.group(2)
        else:
            raise ValueError(f"Invalid file URI: {file_uri}")


    def document_url_to_id(self, document_url):
        file_id, doc_number = self.extract_file_id_and_doc_number(document_url)
        return f'{file_id}_{doc_number}'

        
    def document_urls_to_ids(self, documents_urls):
        documents_ids = []
        for document_url in documents_urls:
            file_id, doc_number = self.extract_file_id_and_doc_number(document_url)
            documents_ids.append(file_id)
        return documents_ids
    
    def get_documents_from_op_sparql_endpoint(self, query, max_attempts=3, retry_delay=5):
        attempt = 0
        results_dict = dict(list())
        while attempt < max_attempts:
            attempt += 1
            self.logger.info(f"Querying op sparql endpoint (attempt {attempt})")
            # use SPARQLWrapper to query the sparql endpoint
            try:
                sparql = SPARQLWrapper(SPARQL_ENDPOINT)
                sparql.setQuery(query)
                sparql.setReturnFormat(CSV)
                results = sparql.query().convert()
                
                if not os.path.exists(self.DATA_DIR):
                    os.makedirs(self.DATA_DIR)
                
                with open(self.documents_to_download_file, 'wb') as f:
                    f.write(results)
                
                return results

            except Exception as err:
                self.logger.error(f"Error querying op sparql endpoint, Error: {err}")
                time.sleep(retry_delay)

        return None
    
    def add_arguments(self, parser):
        parser.add_argument('--query_file', type=str, default=None, help="Path to the file containing the SPARQL query")
        parser.add_argument('--query', type=str, default=None, help="String containing the SPARQL query")
        parser.add_argument('--year', type=int, default=0, help="Documents creation year")
        parser.add_argument('--month', type=int, default=0, help="Documents creation month")
        parser.add_argument('--day', type=int, default=0, help="Documents creation day")
        parser.add_argument('--date1', type=str, default=0, help="Documents creation between date1 and")
        parser.add_argument('--date2', type=str, default=0, help="date2")
        
        parser.add_argument('--limit', type=int, default=0, help="Limit number of returned documents by sparql query")
        return parser.parse_args()
    
    def process_command_line_arguments(self):
        parser = argparse.ArgumentParser(description='Process documents')
        self.add_arguments(parser)
        super().add_arguments(parser)
        return parser.parse_args()
    
    def get_documents_created_between_dates_query(self,start_date, end_date,limit = 0):
        query = f"""
            prefix cdm: <http://publications.europa.eu/ontology/cdm#>
            prefix cmr: <http://publications.europa.eu/ontology/cdm/cmr#>
            prefix eurovoc: <http://eurovoc.europa.eu/schema#> 
            prefix xsd: <http://www.w3.org/2001/XMLSchema#>

            select distinct ?doc_url ?work ?mime ?lastModificationDate ?creationDate ?title
            where {{ 
                ?work cdm:work_id_document ?work_id. 
                ?exp cdm:expression_belongs_to_work ?work . 
                ?work cdm:work_is_about_concept_eurovoc ?eurovoc.
                ?exp cdm:expression_title ?title. 
                ?exp cdm:expression_uses_language <http://publications.europa.eu/resource/authority/language/ENG>. 
                BIND(lang(?title) as ?lg2). 
                BIND( if(?lg2="", "en", ?lg2) AS ?lang). 
                FILTER(?lang="en"). 
                ?uri_manif cdm:manifestation_manifests_expression ?exp; 
                        cdm:manifestation_type ?format. 
                ?work cmr:creationDate ?creationDate.
                optional{{?work cmr:lastModificationDate ?lastModificationDate. }}
                FILTER (BOUND(?creationDate) && ?creationDate >= "{start_date}"^^xsd:date && ?creationDate <= "{end_date}"^^xsd:date)
                ?doc_url cdm:item_belongs_to_manifestation ?uri_manif. 
                ?doc_url cmr:manifestationMimeType ?mime. 
                FILTER(contains(?mime,"pdf"))
            }}
        """
        if limit > 0:
            query += f" limit {limit}"
        
        return query
    
    def get_documents_by_date_query(self,year, month, day, limit=0):
        
        # Determine the date filter condition based on month and day
        if month == 0:
            date_filter = f"year(?creationDate) = {year}"
        elif day == 0:
            date_filter = f"year(?creationDate) = {year} && month(?creationDate) = {month}"
        else:
            date_filter = f"year(?creationDate) = {year} && month(?creationDate) = {month} && day(?creationDate) = {day}"
        
        query = f"""
            prefix cdm: <http://publications.europa.eu/ontology/cdm#>
            prefix cmr: <http://publications.europa.eu/ontology/cdm/cmr#>
            prefix eurovoc: <http://eurovoc.europa.eu/schema#> 
            prefix xsd: <http://www.w3.org/2001/XMLSchema#>

            select distinct ?doc_url ?work ?mime ?lastModificationDate ?creationDate ?title
            where {{ 
                ?work cdm:work_id_document ?work_id. 
                ?exp cdm:expression_belongs_to_work ?work . 
                ?work cdm:work_is_about_concept_eurovoc ?eurovoc.
                ?exp cdm:expression_title ?title. 
                ?exp cdm:expression_uses_language <http://publications.europa.eu/resource/authority/language/ENG>. 
                BIND(lang(?title) as ?lg2). 
                BIND( if(?lg2="", "en", ?lg2) AS ?lang). 
                FILTER(?lang="en"). 
                ?uri_manif cdm:manifestation_manifests_expression ?exp; 
                        cdm:manifestation_type ?format. 
                ?work cmr:creationDate ?creationDate.
                optional{{?work cmr:lastModificationDate ?lastModificationDate. }}
                FILTER (BOUND(?creationDate) && {date_filter})
                ?doc_url cdm:item_belongs_to_manifestation ?uri_manif. 
                ?doc_url cmr:manifestationMimeType ?mime. 
                FILTER(contains(?mime,"pdf"))
            }}"""
        
        if limit > 0:
            query += f" limit {limit}"
        
        return query

    def construct_query(self, args, handler):
        """
        Construct a query based on command-line arguments and determine the filename to save results.

        Parameters:
        - args: The command-line arguments object.
        - handler: An instance of GetDocumentToDownloadHandler.

        Returns:
        - A tuple containing the constructed query string and the filename.
        """

        filename = None  # Initialize filename to None

        if args.query_file:
            with open(args.query_file, "r") as file:
                query = file.read()
            filename = args.query_file  # Use the same file to save the results
        elif args.query:
            query = args.query
            # Create a filename using the hash of the query
            hash_object = hashlib.md5(query.encode())
            filename = hash_object.hexdigest() + ".csv"
        elif args.date1 and args.date2:
            query = handler.get_documents_created_between_dates_query(args.date1, args.date2)
            filename = f"{args.date1}_{args.date2}.csv"
        elif any(getattr(args, attr_name) for attr_name in ["year", "month", "day"]):
            year = getattr(args, 'year', 0)
            month = getattr(args, 'month', 0)
            day = getattr(args, 'day', 0)
            limit = getattr(args, 'limit', 0)

            if year == 0:
                raise ValueError("Year cannot be 0")

            query = handler.get_documents_by_date_query(year, month, day, limit)
            filename = f"{year}_{month}_{day}.csv"
        else:
            current_date = datetime.datetime.now()
            query = handler.get_documents_by_date_query(current_date.year, current_date.month - 1, 0)
            filename = f"current_{current_date.year}_{current_date.month - 1}.csv"

        return query, filename

# In your main function
def main():
    get_docs_to_download_hd = GetDocumentToDownloadHandler()
    args = get_docs_to_download_hd.process_command_line_arguments()

    query,results_file_name = get_docs_to_download_hd.construct_query(args, get_docs_to_download_hd)
    
    return results_file_name
    
    print(query)
    exit()
    
    get_docs_to_download_hd.logger.info(f"query => {query}")

    docs_to_download = get_docs_to_download_hd.get_documents_from_op_sparql_endpoint(query)

    if not docs_to_download:
        raise ValueError('Error querying sparql endpoint')


if __name__ == "__main__":

    main()

