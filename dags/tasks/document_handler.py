import os
import logging
import csv
import argparse
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor, as_completed

from minio import Minio
from minio.error import MinioException

import dotenv
import os

dotenv.load_dotenv(dotenv.find_dotenv())

LOG_FILE_NAME = 'document_handler.log'

class DocumentHandler:
    BASE_DIR = os.path.dirname(os.path.realpath(__file__))
    DATA_DIR = os.path.join(BASE_DIR, '..', 'data')
    LOG_DIR = os.path.join(BASE_DIR, '..', 'logs')
    DOWNLOADED_DOCUMENTS_DIR = os.path.join(DATA_DIR, 'downloaded_documents')

    PROCESSED_DOCUMENTS_BUCKET = 'processed-documents-bucket'
    DOWNLOADED_DOCUMENTS_BUCKET = 'downloaded-documents-bucket'
    REDUCED_DOCUMENTS_BUCKET = 'reduced-documents-bucket'
    DOCS_TO_DOWNLOAD_FILE = "documents_to_download.csv"

    def __init__(self):

        self.logger = self.setup_logging(LOG_FILE_NAME)

        self.setup_minio()

    def setup_logging(self, LOG_FILE_NAME):
            
        logger = logging.getLogger(__file__)
        
        os.makedirs(self.LOG_DIR, exist_ok=True)
        log_file_path = os.path.join(self.LOG_DIR, LOG_FILE_NAME)
        
        # Create a file handler for writing logs to a file
        file_handler = logging.FileHandler(log_file_path)
        file_handler.setLevel(logging.INFO)
        
        # Create a stream handler for writing logs to the console
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.INFO)
        
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        stream_handler.setFormatter(formatter)
        
        logger.addHandler(file_handler)
        logger.addHandler(stream_handler)
        
        return logger



    def setup_minio(self):
        try : 
            port = os.getenv('MINIO_PORT_2')
            access_key = os.getenv('MINIO_ACCESS_KEY')
            secret_key = os.getenv('MINIO_SECRET_KEY')
            secure = False

            host = 'localhost'
            if os.path.exists('/.dockerenv'):
                host = os.getenv('MINIO_HOST')

            endpoint = f"{host}:{port}"
            
            self.minioClient = Minio(endpoint=endpoint,
                                access_key=access_key,
                                secret_key=secret_key,
                                secure=secure)
            
            self.create_buckets_if_not_exist([
                DocumentHandler.DOWNLOADED_DOCUMENTS_BUCKET,
                DocumentHandler.PROCESSED_DOCUMENTS_BUCKET,
                DocumentHandler.REDUCED_DOCUMENTS_BUCKET
            ])
                
        except MinioException as err:
            self.logger.error("Minio Init Error: {}".format(err))
            raise err

    
    def create_buckets_if_not_exist(self, bucket_names):
        for bucket_name in bucket_names:
            found = self.minioClient.bucket_exists(bucket_name)
            if not found:
                self.minioClient.make_bucket(bucket_name)


    def read_csv(self, file_path):
        data = []
        with open(file_path, 'r') as f:
            csv_reader = csv.reader(f)
            for row in csv_reader:
                print(row[0])
                data.append(row[0])
                break
        return data

    def write_csv(self, file_path, data):
        with open(file_path, 'w', newline='') as f:
            csv_writer = csv.writer(f)
            for row in data:
                csv_writer.writerow([row])

    def process_documents_list(self, documents_list, process_function, max_workers, *args, **kwargs):
        progress_bar = tqdm(total=len(documents_list), desc="Processing Documents", unit="document")
        with ThreadPoolExecutor(max_workers=max_workers) as executor:
            futures = [executor.submit(process_function, document, *args, **kwargs) for document in documents_list]
            for future in as_completed(futures):
                future.result()
                progress_bar.update(1)
        progress_bar.close()
    
    
    def add_arguments(self, parser):
        parser.add_argument('--documents_urls', type=str, default="", help="Path to the CSV file containing file IDs")
        parser.add_argument('--num_documents', type=int, default=0, help="Number of documents to handle")
        parser.add_argument('--max_attempts', type=int, default=3, help="Maximum number of download attempts")
        parser.add_argument('--retry_delay', type=int, default=5, help="Delay in seconds between download retries")
        parser.add_argument('--max_workers', type=int, default=1, help="The number of parallel workers")

        # return parser.parse_args()

    def process_command_line_arguments(self):
        parser = argparse.ArgumentParser(description="Handle files")
        self.add_arguments(parser)
        return parser.parse_args()