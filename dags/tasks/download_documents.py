import csv
import io
import os
import requests
import time
import re


from minio.error import S3Error

try:
    from tasks.document_handler import DocumentHandler
except ImportError:
    from document_handler import DocumentHandler

LOG_FILE_NAME = 'download_documents.log'



class DownloadDocumentHandler(DocumentHandler):
    def __init__(self):
        super().__init__()
        self.logger = self.setup_logging(LOG_FILE_NAME)
        

    def download_file(self, file_uri, max_attempts=3, retry_delay=5):
        file_id, doc_number = self.extract_file_id_and_doc_number(file_uri)
        file_name = f"{file_id}_{doc_number}.pdf"
        file_path = os.path.join(self.DOWNLOADED_DOCUMENTS_DIR, file_name)

        # Check if the file already exists
        try:
            self.minioClient.stat_object(DocumentHandler.DOWNLOADED_DOCUMENTS_BUCKET, file_name)
            self.logger.info(f"File already downloaded: {file_id}")
            return file_path, None
        
        except S3Error as err:
            pass

        attempt = 0
        while attempt < max_attempts:
            attempt += 1
            self.logger.info(f"Downloading file (attempt {attempt}): {file_id}")
            # TODO: fix : get full file url from input
            try:
                url = f"http://publications.europa.eu/resource/cellar/{file_id}/{doc_number}"
                response = requests.get(url)

                if response.status_code == 200:
                    content_length = int(response.headers.get("Content-Length", 0))
                    downloaded_length = len(response.content)

                    if downloaded_length == 0:
                        self.logger.error(f"Error empty file, {file_id}")
                        return None, None

                    try:
                        self.minioClient.put_object(DocumentHandler.DOWNLOADED_DOCUMENTS_BUCKET, file_name, io.BytesIO(response.content), len(response.content))
                    
                    except S3Error as err:
                            self.logger.error(f"Error uploading file to minio, file : {file_id}, Error: {err}")
                        
                    self.logger.info(f"Downloaded file: {file_id}")
                    return file_path, None

                else:
                    self.logger.warning(f"Warning {response.status_code}, {file_id} (attempt {attempt})")
                    time.sleep(retry_delay)

            except requests.exceptions.RequestException as e:
                self.logger.error(f"RequestException: {e}, {file_id} (attempt {attempt})")
                time.sleep(retry_delay)

        self.logger.error(f"Failed to download file after {max_attempts} attempts: {url}")
        return None, None

    def extract_file_id_and_doc_number(self, file_uri):
        match = re.match(r"http://publications.europa.eu/resource/cellar/([^/]+)/([^/]+)", file_uri)
        if match:
            return match.group(1), match.group(2)
        else:
            raise ValueError(f"Invalid file URI: {file_uri}")

    def list_of_downloaded_documents_ids(self, downloaded_documents=None):
        
        if downloaded_documents is None:
            downloaded_documents = DocumentHandler.DOWNLOADED_DOCUMENTS_BUCKET

        objects = self.minioClient.list_objects(downloaded_documents)
        documents_ids = set([object.object_name.rsplit('.', 1)[0] for object in objects if object.object_name.endswith('.pdf')])
        return documents_ids

    def document_url_to_id(self, document_url):
        file_id, doc_number = self.extract_file_id_and_doc_number(document_url)
        return f'{file_id}_{doc_number}'
    
    def get_documents_url(self, file_path):
        data = []
        with open(file_path, 'r') as f:
            csv_reader = csv.reader(f)
            next(csv_reader)
        
            for row in csv_reader:
                data.append(row[0])
        return data

    def determine_files_to_download(self,documents_urls):
        downloaded_documents_ids = self.list_of_downloaded_documents_ids()
        documents_urls_to_download = [document_url for document_url in documents_urls if self.document_url_to_id(document_url) not in downloaded_documents_ids]
        return documents_urls_to_download  
    
    def download_files(self, file_ids, max_workers, max_attempts=3, retry_delay=5):
        self.logger.info(f"Downloading {len(file_ids)} files")
        self.process_documents_list(file_ids, self.download_file, max_workers, max_attempts, retry_delay)
    
    def add_arguments(self, parser):
        super().add_arguments(parser)
        
    def document_urls_to_ids(self, documents_urls):
        documents_ids = []
        for document_url in documents_urls:
            file_id, doc_number = self.extract_file_id_and_doc_number(document_url)
            documents_ids.append(file_id)
        return documents_ids

def main():
    download_documents_handler = DownloadDocumentHandler()
    args = download_documents_handler.process_command_line_arguments()
    
    if args.documents_urls and args.documents_urls != 'None':
        if args.num_documents == 0:
            docs_to_download_urls = download_documents_handler.read_csv(args.documents_urls)
        else:
            docs_to_download_urls = download_documents_handler.read_csv(args.documents_urls)[:args.num_documents]
    # if file DownloadDocumentHandler.DOCS_TO_DOWNLOAD_FILE exists  
    else :
        documents_to_download_file = os.path.join(download_documents_handler.DATA_DIR, DocumentHandler.DOCS_TO_DOWNLOAD_FILE)
        # get only first column
        if os.path.exists(documents_to_download_file):
            docs_to_download_urls = download_documents_handler.get_documents_url(documents_to_download_file)
        else :
            exit("No document IDs provided")

    print(f"downloading ====> {len(docs_to_download_urls)}")
    documents_ids_to_download = download_documents_handler.determine_files_to_download(docs_to_download_urls)
    
    

    download_documents_handler.download_files(documents_ids_to_download, args.max_workers, args.max_attempts, args.retry_delay)

if __name__ == "__main__":
    
    main()

