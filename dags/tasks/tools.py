import os
import urllib.request
from subprocess import Popen, PIPE

import re
from itertools import groupby
from textacy import preprocessing

class Tools(object):

    @staticmethod 
    def download_tika_jar(jar_url, jar_path):
        urllib.request.urlretrieve(jar_url, jar_path)

    @staticmethod
    def process_document_using_tika(file_name,file_content, logger, jar_path="tika-app-2.6.0.jar"):
        if not os.path.exists(jar_path):
            logger.info("Tika JAR not found. Downloading...")
            Tools.download_tika_jar("https://repo1.maven.org/maven2/org/apache/tika/tika-app/2.6.0/tika-app-2.6.0.jar", jar_path)

        p = Popen(
            [
                "java",
                "-Xms8g",
                "-Xmx8g",
                "-jar",
                jar_path,
                "--text",
            ],
            stdout=PIPE,
            stdin=PIPE,
            stderr=PIPE,
        )

        errs = None
        try:
            outs, errs = p.communicate(input=file_content, timeout=300)
            text = outs.decode('utf-8')

            return text

        except Exception as e:
            logger.error(f"Error processing file: {file_name}, Error: {str(e)}, Tika errors: {errs}", exc_info=True)
    
    
    @staticmethod
    def clean(text):
        if not text:
            return ""
        if len(text) < 2:
            return ""
        r = re.compile(r"^\d*[.,]?\d*$")
        lam = lambda x: x if r.match(x) else x.replace(".", " . ").replace(",", " , ")
        my_string = text.replace(chr(0), "")
        my_string = (
            my_string.replace("- \n ", "")
            .replace("- \n", "")
            .replace("-\n ", "")
            .replace("-\n", "")
        )
        my_string = my_string.replace(";", " ; ")
        my_string = my_string.replace("'", " ")
        my_string = my_string.replace("%", " %")
        my_string = my_string.replace("(", " ( ")
        my_string = my_string.replace(")", " ) ")
        my_string = my_string.replace("[", " [ ")
        my_string = my_string.replace("]", " ] ")
        my_string = my_string.replace("|", " | ")
        my_string = my_string.replace("{", " { ")
        my_string = my_string.replace("}", " } ")
        my_string = my_string.replace("?", " ? ")
        my_string = my_string.replace("//", "/").replace("//", "/")
        my_string = my_string.replace("e.g.", "eg")
        my_string = my_string.replace("i.e.", "ie")
        my_string = my_string.replace("e. g.", "eg")
        my_string = my_string.replace("i. e.", "ie")
        my_string = my_string.replace(".", " . ").replace(":", " : ").replace('"', ' " ')
        my_string = "".join(
            "".join(s)[:3] for _, s in groupby(my_string)
        )  # remove more than 3 repetitions
        my_string = " " + " ".join([lam(w) for w in my_string.split()]) + " "
        my_string = (
            my_string.replace(" /", " ")
            .replace("/ ", " ")
            .replace(" -", " ")
            .replace("- ", " ")
            .replace("_ , _", "_,_")
        )
        my_string = re.sub("\s+", " ", my_string).strip()
        my_string = "".join(
            c
            for c in my_string.lower()
            if c in "abcdefghijklmnopqrstuvwxyz0123456789/-_&@ \n"
        )
        my_string = " ".join(
            [
                w
                for w in my_string.split(" ")
                if w
                and not w.replace("/", "")
                .replace("_", "")
                .replace("-", "")
                .replace("&", "")
                .replace("@", "")
                .isdigit()
            ]
        )
        return my_string
    
    @staticmethod
    def sentenced(text):
        preproc = preprocessing.make_pipeline(
        preprocessing.remove.accents,
        preprocessing.replace.urls,
        preprocessing.replace.emails,
        preprocessing.replace.phone_numbers,
        )
        return Tools.clean(preproc(text))