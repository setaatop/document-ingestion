from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from tasks import download_documents, get_documents_to_download, process_documents, save_documents_to_seta
import os
import sys
import shlex


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 7, 7),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    
}

dag = DAG(
    'cellar_seta_dag',
    default_args=default_args,
    description='A DAG to download, process, and save CELLAR data to SeTA',
    schedule_interval=timedelta(days=1),
    params={
        "docs_to_download_query": "",
        "max_workers_download": 1,  
        "max_workers_process": 1,
        "max_workers_save_seta": 1,
        "document_created_between_date1": None,  
        "document_created_between_date2": None,
        "document_created_year": int(0),  
        "document_created_month": int(0),    
        "document_created_day": int(0),
        "number_documents_to_download": int(0)
    },
)

# full path to the download script
current_dir = os.path.dirname(os.path.abspath(__file__))

get_documents_to_download_script = os.path.join(current_dir, 'tasks/get_documents_to_download.py')

# if there is a passed query param, read it and save it to a file. 
# Then, pass it to the script

def add_arg_to_argv(arg_name, value, argv):
    """Helper function to add argument to argv list if value is valid."""
    if value not in (None, 'None', '', 0, '0'):
        argv.extend([arg_name, str(value)])

def get_documents_to_download_task_wrapper(query='None',
                                           document_created_between_date1='None',
                                           document_created_between_date2='None',
                                           document_created_year='None',
                                           document_created_month='None',
                                           document_created_day='None',
                                           number_documents_to_download='None'):

    # Initialize the list for argv arguments
    argv = ['get_documents_to_download']

    # Use helper function to simplify the process of adding args to argv
    add_arg_to_argv('--query', query, argv)
    add_arg_to_argv('--document_created_between_date1', document_created_between_date1, argv)
    add_arg_to_argv('--document_created_between_date2', document_created_between_date2, argv)
    add_arg_to_argv('--year', document_created_year, argv)
    add_arg_to_argv('--month', document_created_month, argv)
    add_arg_to_argv('--day', document_created_day, argv)
    add_arg_to_argv('--limit', number_documents_to_download, argv)

    # Set sys.argv and call the main function
    sys.argv = argv
    
    get_documents_to_download.main()


get_documents_to_download_task = PythonOperator(
    task_id='get_documents_to_download',
    provide_context=True,
    python_callable=get_documents_to_download_task_wrapper,
    op_args=[
         "{{ params.docs_to_download_query }}",
          "{{ params.document_created_between_date1 }}",
          "{{ params.document_created_between_date2 }}",
          
          "{{ params.document_created_year }}",
          "{{ params.document_created_month }}",
          "{{ params.document_created_day }}",
          
          "{{ params.number_documents_to_download }}",
     ],
    dag=dag,
)



def download_task_wrapper(documents_urls, num_documents, max_attempts, retry_delay, max_workers):
    sys.argv = ['download_documents', 
                '--documents_urls', documents_urls, 
                '--num_documents', str(num_documents),
                '--max_attempts', str(max_attempts), 
                '--retry_delay', str(retry_delay), 
                '--max_workers', str(max_workers)]
    
    download_documents.main()


download_task = PythonOperator(
    task_id='download',
    provide_context=True,
    python_callable=download_task_wrapper,
    op_kwargs={
        'documents_urls': None,
        'num_documents': 0,
        'max_attempts': 3,
        'retry_delay': 5,
        'max_workers': "{{ params.max_workers_download }}"
    },
    dag=dag,
)

# Wrapper for the process task
def process_task_wrapper(max_workers):
    sys.argv = ['process_documents', '--max_workers', str(max_workers)]
    # Assuming the main function in process_documents.py is named "main"
    process_documents.main()

process_task = PythonOperator(
    task_id='convert_documents_to_text',
    provide_context=True,
    python_callable=process_task_wrapper,
    op_kwargs={
        'max_workers': "{{ params.max_workers_process }}"
    },
    dag=dag,
)

# Wrapper for the save_documents_to_seta task
def save_documents_to_seta_wrapper(max_workers):
    sys.argv = ['save_documents_to_seta', '--max_workers', str(max_workers)]
    
    save_documents_to_seta.main()

save_document_in_seta_script = PythonOperator(
    task_id='save_to_seta',
    provide_context=True,
    python_callable=save_documents_to_seta_wrapper,
    op_kwargs={
        'max_workers': "{{ params.max_workers_save_seta }}"
    },
    dag=dag,
)


get_documents_to_download_task >> download_task >> process_task >> save_document_in_seta_script